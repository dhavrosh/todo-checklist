const app = require('./server/app');
const Todo = require('./server/todos/tests');

before(done => app.listen(done));

describe('TodoServer', () => {
    it('started successfully', () => {
        describe(Todo.name, () => {
            Todo.tests.forEach(test => {
                describe(test.name, () => it(test.description, test.worker))
            });
        });
    });
});

after(done => app.close(done));
