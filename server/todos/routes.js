const express = require('express');
const todoCtrl = require('./ctrl');

const router = express.Router();

router.get('/', todoCtrl.getAll);
router.post('/', todoCtrl.create);
router.delete('/:id', todoCtrl.remove);

router.get('/done/:id', todoCtrl.markDone);
router.get('/undone/:id', todoCtrl.markUndone);

module.exports = router;