const assert = require('assert');
const axios = require('axios');
const Todo = require('mongoose').model('Todo');

const todoObj = {
    title: 'Test',
    done: true
};

const baseOfServer = process.env.BASE || 'http://localhost:3000';

function getAll(done) {
    let id;

    Todo.create(todoObj)
        .then(todo => {
            id = todo._id;
            return axios.get(`${baseOfServer}/todo`)
        })
        .then(response => {
            const todos = response.data;

            assert.equal(Array.isArray(todos), true);
            assert.equal(todos.length > 0, true);

            return Todo.findByIdAndRemove(id);
        })
        .then(() => done())
        .catch(err => done(err));
}

function create(done) {
    axios.post(`${baseOfServer}/todo`, todoObj)
        .then(response => {
            const todo = response.data;

            assert.equal(todo.title, todoObj.title);
            assert.equal(todo.done, todoObj.done);

            return Todo.findByIdAndRemove(todo._id);
        })
        .then(() => done())
        .catch(err => done(err));
}

function remove(done) {
    Todo.create(todoObj)
        .then(todo => axios.delete(`${baseOfServer}/todo/${todo._id}`))
        .then(response => {
            const todo = response.data;

            assert.equal(todo.title, todoObj.title);
            assert.equal(todo.done, todoObj.done);

            return done();
        })
        .catch(err => done(err));
}

function markDone(done) {
    todoObj.done = false;

    Todo.create(todoObj)
        .then(todo => axios.get(`${baseOfServer}/todo/done/${todo._id}`))
        .then(response => {
            const todo = response.data;

            assert.equal(todo.done, true);

            return Todo.findByIdAndRemove(todo._id);
        })
        .then(() => done())
        .catch(err => done(err));
}

function markUndone(done) {
    todoObj.done = true;

    Todo.create(todoObj)
        .then(todo => axios.get(`${baseOfServer}/todo/undone/${todo._id}`))
        .then(response => {
            const todo = response.data;

            assert.equal(todo.done, false);

            return Todo.findByIdAndRemove(todo._id);
        })
        .then(() => done())
        .catch(err => done(err));
}

module.exports = {
    name: 'TodoCtrl',
    tests: [
        {
            name: 'getAll',
            description: 'should return all existing todos',
            worker: getAll,
        },
        {
            name: 'create',
            description: 'should create correct todo',
            worker: create,
        },
        {
            name: 'remove',
            description: 'should remove selected todo',
            worker: remove,
        },
        {
            name: 'markDone',
            description: 'should mark selected todo as done',
            worker: markDone,
        },
        {
            name: 'markUndone',
            description: 'should mark selected todo as undone',
            worker: markUndone,
        }
    ]
};