const Todo = require('mongoose').model('Todo');
const sendJSON = require('./utils').sendJSON;

function getAll(req, res) {
    return Todo.find({})
        .then(todos => sendJSON(res, 200, todos))
        .catch(err => sendJSON(res, 500, err.message));
}

function create(req, res) {
    const todo = req.body;

    if (todo.title) {
        return Todo.create(todo)
            .then(todo => sendJSON(res, 200, todo))
            .catch(err => sendJSON(res, 500, err.message));
    } else {
        sendJSON(res, 500, 'Title for Todo is required');
    }
}

function remove(req, res) {
    const id = req.params.id;

    if (id) {
        return Todo.findByIdAndRemove(id)
            .then(todo => sendJSON(res, 200, todo))
            .catch(err => sendJSON(res, 500, err.message));
    } else {
        sendJSON(res, 500, 'Id for Todo is required');
    }
}

function markDone(req, res) {
    const id = req.params.id;

    if (id) {
        return Todo.findByIdAndUpdate(id, { done: true }, { new: true })
            .then(todo => sendJSON(res, 200, todo))
            .catch(err => sendJSON(res, 500, err.message));
    } else {
        sendJSON(res, 500, 'Id for Todo is required');
    }
}

function markUndone(req, res) {
    const id = req.params.id;

    if (id) {
        return Todo.findByIdAndUpdate(id, { done: false }, { new: true })
            .then(todo => sendJSON(res, 200, todo))
            .catch(err => sendJSON(res, 500, err.message));
    } else {
        sendJSON(res, 500, 'Id for Todo is required');
    }
}

module.exports = {
    getAll,
    create,
    remove,
    markDone,
    markUndone
};