const mongoose = require('mongoose');

const todoSchema = new mongoose.Schema({
    title: { type: String, required: true },
    done: { type: Boolean, default: false }
});

mongoose.model('Todo', todoSchema);