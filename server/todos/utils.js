exports.sendJSON = (res, code, content) => {
    res.status(code).json(content);
};