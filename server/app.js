const http = require('http')
const express = require('express');
const bodyParser = require('body-parser');
const db = require('./db');
const todoRouter = require('./todos/routes');

const app = express();
const port = process.env.PORT || 3000;
const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/todo', todoRouter);

const server = http.createServer(app);

exports.listen = done => db.connect(mongoUrl, () => server.listen(port, done));
exports.close = done => server.close(done);