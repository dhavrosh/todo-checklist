const mongoose = require('mongoose');

require('../todos/schema');

mongoose.Promise = global.Promise;

exports.connect = (url, done) => {
    mongoose.connect(url, done);
};